import React from "react";

const Like = ({ onClick, liked }) => {
  let classes = "fa fa-heart";
  if (!liked) classes += "-o";
  return (
    <i
      onClick={() => onClick()}
      className={classes}
      style={{ cursor: "pointer" }}
    />
  );
};

export default Like;
